import random
import numpy as np
import datasets

if __name__ == '__main__':


    # train_clips
    train_user_clips = np.load('./data/traindata/video_clip_user.npy', allow_pickle=True).tolist()
    # select videos
    train_videos = datasets.load_train_video_list()
    random.shuffle(train_videos)
    validation_videos = train_videos[:100]
    np.save('./data/validation_videos.npy', validation_videos)

    # generate gt
    # youtubeid:{userid: clips}
    video_user_clips = datasets.load_user_video_clips()

    # userid:{youtubeid, clips}
    # validation_data: userid:youtubeid
    # groundtruth

    user_video_clips = dict()
    for youtubeid in validation_videos:
        user_clips = video_user_clips[youtubeid]
        for user, clips in user_clips.items():
            if user not in user_video_clips:
                user_video_clips[user] = dict()
            user_video_clips[user][youtubeid] = clips

    validation_data = dict()
    groundtruth = dict()

    for user, video in user_video_clips.items():
        video_keys = list(video.keys())
        random.shuffle(video_keys)
        video_id = video_keys[0]
        clips = video[video_id]
        groundtruth[user] = clips
        validation_data[user] = youtubeid

    np.save('./data/validation_data.npy', validation_data)
    np.save('./data/validation_groundtruth.npy', groundtruth)






