import math
import os
import numpy as np
import evaluate
import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.logging.set_verbosity(tf.logging.ERROR)
from datasets import *
from fnn import fnn
from svm import RankSVM


class Inferencer:

    def __init__(self, model='fnn', weight=1.0):
        self.model = model
        self.weight = weight
        self.is_fnn_init = False
        self.is_svm_init = False
        self.sess = None

    def _init_fnn(self):
        self.clip_input = tf.placeholder("float32", [None, 4096])
        self.history_input = tf.placeholder("float32", [None, 4096])
        with tf.variable_scope('fnn') as scope:
            score = fnn(self.clip_input, self.history_input)

        history_vector_helper = History()
        history_vector_helper.load_data()
        self.history_vectors = history_vector_helper.obtain_average_history()
        self.score = score
        self.is_fnn_init = True

    def _inference_fnn(self, segment, user_ids):
        """
        Parameters
        ----------
        segment: np.array (N * D)
        user_ids: list
        """
        if not self.is_fnn_init:
            self._init_fnn()
            self.sess = tf.Session()
            saver = tf.train.Saver(tf.global_variables())
            self.sess.run(tf.global_variables_initializer())
            saver.restore(self.sess, './save_model/fnn_model_epoch_47')

        history_features = np.take(self.history_vectors, user_ids, axis=0)
        score = self.sess.run(self.score, feed_dict={self.clip_input: segment, self.history_input: history_features})
        return score

    def _init_svm(self):
        svm = RankSVM()
        svm.load_model('./save_model/svm.pkl')
        history_vector_helper = History()
        history_vector_helper.load_data()
        self.svm = svm
        self.history_vector_helper = history_vector_helper
        self.is_svm_init = True
        self.svm.trained = True

    def _inference_svm(self, segment, user_ids):
        """
        Parameters
        ----------
        segment: np.array (N * D)
        user_ids: list
        """
        if not self.is_svm_init:
            self._init_svm()
        features = [self.history_vector_helper.distance_vector(uid, pos[np.newaxis])
                    for uid, pos in zip(user_ids, segment)]
        features = np.concatenate(features, axis=0)
        score = self.svm.predict(features)
        return score

    def inference(self, segment, user_ids):
        if self.model == 'fnn':
            return self._inference_fnn(segment, user_ids)
        elif self.model == 'svm':
            return self._inference_svm(segment, user_ids)
        else:
            score_fnn = self._inference_fnn(segment, user_ids)
            score_svm = self._inference_svm(segment, user_ids)
            score = score_fnn + score_svm * self.weight
            return score


def get_gt_score_personized(ground_truth, test_data,
                            video_information):
    """
     This function shows how a method can be evaluated personized
    """
    gt_score = {}  # ground_truth matrix
    for key in ground_truth.keys():
        video = test_data[key]
        fps = video_information[video]['fps']
        frames = video_information[video]['frames']
        gt_score_tmp = np.zeros(frames)  # chushihua:0
        for i in range(len(ground_truth[key])):
            start_frame = int(ground_truth[key][i][0] * fps)
            end_frame = min(int(ground_truth[key][i][1] * fps), frames)
            gt_score_tmp[start_frame:end_frame] = 1
        gt_score[key] = gt_score_tmp
    return gt_score


def get_testclip(test_video, video_information):

    video_key = {}
    for video in test_video:
        list_tmp = []
        duration = video_information[video]['duration']
        max_k = int(duration / 5)
        if duration % 5 == 0:
            max_k = max_k - 1
        for k in range(max_k + 1):
            list_tmp.append(video + '+' + str(5 * k))
        video_key[video] = list_tmp
    return video_key


def test_personized(inferencer, user_id, test_video, ground_truth,
                    test_data, video_information, features):
    """

    Parameters
    ----------
    inferencer: inference engine, call Infernce
    user_id:
    test_data:
    video_information:
    test_dataset: datasets.TestDataset
    """
    test_clip = get_testclip(test_video, video_information)
    gt_score = get_gt_score_personized(ground_truth, test_data, video_information)  # personized
    all_ap = np.zeros(len(gt_score))
    all_msd = np.zeros(len(gt_score))
    k = 0

    # only keys
    for key in gt_score.keys():
        user_list = []
        clip_features = []
        u_tmp = user_id[key]
        video = test_data[key]
        clip_keys = test_clip[video]
        frames = video_information[video]['frames']
        fps = video_information[video]['fps']
        score_tmp = np.zeros(frames)

        for i in range(len(clip_keys)):
            if clip_keys[i] not in features.keys():
                clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                user_list.append([u_tmp])
            else:
                clip_features.append(features[clip_keys[i]])
                user_list.append([u_tmp])
        user_list = np.asarray(user_list).flatten()
        clip_features = np.reshape(np.asarray(clip_features), [-1, 4096]).astype(np.float32)
        if clip_features.shape[0] != 0:
            _test_score = inferencer.inference(clip_features, user_list)
        else:
            _test_score = np.zeros((clip_features.shape[0], 1), dtype=np.float32)

        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2
                #        test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        test_score_tmp = _test_score
        for i in range(len(clip_keys)):
            start_frame = min(int(i * 5 * fps), frames)
            end_frame = min(int((i + 1) * 5 * fps), frames)
            score_tmp[start_frame:end_frame] = test_score_tmp[i][0]
        score_fnn = score_tmp
        score_gt = gt_score[key]
        all_ap[k] = evaluate.get_ap(score_gt, score_fnn)
        all_msd[k] = evaluate.meaningful_summary_duration([score_gt], score_fnn)
        k += 1
    print('AP=%.2f%%; MSD=%.2f%%' % (100 * np.mean(all_ap), 100 * np.mean(all_msd)))
    return round(100 * np.mean(all_ap), 2), round(100 * np.mean(all_msd), 2)


def generate_hr_ndcg(inferencer, user_id, test_video, ground_truth,
                     test_data, video_information, video_user_clips, features):
    test_clip = get_testclip(test_video, video_information)
    gt_score = get_gt_score_personized(ground_truth, test_data, video_information)  # key is user
    user_list = []
    pos_prediction = {}
    neg_prediction = {}

    for key in gt_score.keys():
        pos_user_list = []
        neg_user_list = []
        tmp_u = user_id[key]
        tmp_pos_prediction = []
        tmp_neg_prediction = []
        video = test_data[key]  # user->
        all_clip_keys = test_clip[video]  # video->all clips(video+time)
        pos_clip_id = video_user_clips[video][key]
        pos_clip_keys = []
        for i in range(len(pos_clip_id)):
            #            pdb.set_trace()
            pos_clip_keys.append(video + '+' + str(pos_clip_id[i][0]))  # add posclip
            if video + '+' + str(pos_clip_id[i][0]) in all_clip_keys:
                all_clip_keys.remove(video + '+' + str(pos_clip_id[i][0]))  # remove posclip

        pos_clip_features = []
        for i in range(len(pos_clip_keys)):
            if pos_clip_keys[i] not in features.keys():
                pos_clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                pos_user_list.append([tmp_u])
            else:
                pos_clip_features.append(features[pos_clip_keys[i]])
                pos_user_list.append([tmp_u])
        pos_clip_features = np.reshape(np.asarray(pos_clip_features), [-1, 4096]).astype(np.float32)
        pos_user_list = np.asarray(pos_user_list).flatten()

        if pos_clip_features.shape[0] != 0:
            _test_score = inferencer.inference(pos_clip_features, pos_user_list)
        else:
            _test_score = np.zeros((pos_clip_features.shape[0], 1), dtype=np.float32)

        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2
                #        if len(_test_score) > 0:
        #            test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        for i in range(len(_test_score)):
            tmp_pos_prediction.append(_test_score[i][0])

        #### neg prediction ####
        neg_clip_keys = all_clip_keys
        neg_clip_features = []
        for i in range(len(neg_clip_keys)):
            if neg_clip_keys[i] not in features.keys():
                neg_clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                neg_user_list.append([tmp_u])
            else:
                neg_clip_features.append(features[neg_clip_keys[i]])
                neg_user_list.append([tmp_u])
        neg_clip_features = np.reshape(np.asarray(neg_clip_features), [-1, 4096]).astype(np.float32)
        neg_user_list = np.asarray(neg_user_list).flatten()

        if neg_clip_features.shape[0] != 0:
            _test_score = inferencer.inference(neg_clip_features, neg_user_list)
        else:
            _test_score = np.zeros((neg_clip_features.shape[0], 1), dtype=np.float32)

        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2

                #        if len(_test_score) > 0:
        #            test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        for i in range(len(_test_score)):
            tmp_neg_prediction.append(_test_score[i][0])
        #### add to dict ####
        user_list.append(key)
        pos_prediction[key] = tmp_pos_prediction
        neg_prediction[key] = tmp_neg_prediction
    return user_list, pos_prediction, neg_prediction


def get_idcg(length):
    idcg = 0.0
    for i in range(length):
        idcg = idcg + math.log(2) / math.log(i + 2)
    return idcg


def compute_hr_ndcg(user_list, pos_prediction, neg_prediction, topk):
    user_map_list = []
    user_nmsd_list = []
    user_hr_list = []
    user_ndcg_list = []
    user_recall_list = []
    for i in range(len(user_list)):
        tmp_user = user_list[i]
        pos_length = len(pos_prediction[tmp_user])
        target_length = min(topk, pos_length)

        pos_value = pos_prediction[tmp_user]
        neg_value = neg_prediction[tmp_user]
        pos_value.extend(neg_value)
        # map and nmsd
        score_predict = np.asarray(pos_value)
        score_predict = (score_predict - min(score_predict)) / np.clip(max(score_predict) - min(score_predict), 1e-8, np.inf)
        score_gt = np.zeros(len(score_predict))
        score_gt[:pos_length] = 1
        user_map_list.append(evaluate.get_ap(score_gt, score_predict))
        user_nmsd_list.append(evaluate.meaningful_summary_duration([score_gt], score_predict))

        tmp_prediction = np.asarray(pos_value)
        sort_index = np.argsort(tmp_prediction)[::-1]  # sort index
        hit_value = 0
        dcg_value = 0
        for idx in range(min(topk, len(sort_index))):
            ranking = sort_index[idx]
            if ranking < pos_length:
                hit_value += 1
                dcg_value += math.log(2) / math.log(idx + 2)
        tmp_hr = round(hit_value / target_length, 4)
        idcg = get_idcg(target_length)
        tmp_dcg = round(dcg_value / idcg, 4)
        tmp_recall = hit_value / pos_length
        user_hr_list.append(tmp_hr)
        user_ndcg_list.append(tmp_dcg)
        user_recall_list.append(tmp_recall)

    mean_map = sum(user_map_list) / len(user_list)
    mean_nmsd = sum(user_nmsd_list) / len(user_list)
    mean_hr = sum(user_hr_list) / len(user_list)
    mean_ndcg = sum(user_ndcg_list) / len(user_list)
    mean_recall = sum(user_recall_list) / len(user_list)
    return round(mean_map, 4), round(mean_nmsd, 4), round(mean_hr, 4), round(mean_ndcg, 4), round(mean_recall, 4)


def load_ground_truth():
    return np.load('./data/testdata/ground_truth.npy', allow_pickle=True).tolist()


def load_valid_ground_truth():
    return np.load('./data/validation_groundtruth.npy', allow_pickle=True).tolist()


def load_features(split):
    if split == 'test':
        return np.load('./data/testdata/test_features_2nor.npy', allow_pickle=True).tolist()
    else:
        return np.load('./data/traindata/all_features_2nor.npy', allow_pickle=True).tolist()


def test(model, split='test'):

    inferencer = Inferencer(model)
    print("[Init Inferencer] type: {}".format(model))

    features = load_features(split)
    user_id = load_user_id()
    video_user_clips = load_user_video_clips()

    if split == 'test':
        test_video = load_test_video_list()
        ground_truth = load_ground_truth()
        test_data = load_test_data()
        video_information = np.load('./data/testdata/video_information.npy', allow_pickle=True).tolist()
    else:
        test_video = load_validation_video_list()
        ground_truth = load_ground_truth()
        test_data = load_validation_data()
        # TODO: update video information
        video_information = np.load('./data/testdata/video_information.npy', allow_pickle=True).tolist()

    for topk in range(1, 6):
        hit_user_list, hit_pos_prediction, hit_neg_prediction = generate_hr_ndcg(inferencer,
                                                                                 user_id,
                                                                                 test_video,
                                                                                 ground_truth,
                                                                                 test_data,
                                                                                 video_information,
                                                                                 video_user_clips,
                                                                                 features)

        my_map, my_nmsd, _hr, _ndcg, _recall = compute_hr_ndcg(hit_user_list, hit_pos_prediction, hit_neg_prediction,
                                                               topk)
        print('test_map:', my_map, 'nmsd:', my_nmsd, 'hr:', _hr, 'ndcg:', _ndcg, 'recall:', _recall)

    _map, _nmsd = test_personized(inferencer, user_id, test_video, ground_truth,
                    test_data, video_information, features)
    print('_map:', _map)
    print('_nmsd:', _nmsd)


if __name__ == '__main__':

    print('[Test] model = SVM')
    print('==================')
    test('svm')
    print('[Test] model = FNN')
    print('==================')
    # test('fnn')
    print('[Test] model = SVM+FNN')
    print('======================')
    # test('svm_fnn')