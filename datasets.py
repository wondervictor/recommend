# dataset
import os
import time
import pickle
import random
import numpy as np
import scipy.sparse as sparse
from scipy.spatial.distance import cdist
from collections import defaultdict


def user_history_average(user_num=6527):
    X = np.load('./data/traindata/X_matrix.npy', allow_pickle=True)
    sparse_index = np.load('./data/traindata/sparse_index.npy', allow_pickle=True)
    sparse_value = np.load('./data/traindata/sparse_value.npy', allow_pickle=True).astype('float32')
    sparse_index_row = sparse_index[:, 0]
    sparse_index_col = sparse_index[:, 1]
    sparse_matrix = sparse.coo_matrix((sparse_value, (sparse_index_row, sparse_index_col)), shape=(user_num, X.shape[0]))
    # 6527 * len(X)
    user_clip = sparse_matrix.todense()
    # 6527 * 4096
    user_history = np.matmul(user_clip, X)
    return user_history


class History:

    def __init__(self, user_num=6527):
        self.user_num = user_num
        self.user_average_history = None
        self.history = None
        self.user_history = defaultdict(list)

    def load_data(self):
        X = np.load('./data/traindata/X_matrix.npy', allow_pickle=True)
        sparse_index = np.load('./data/traindata/sparse_index.npy', allow_pickle=True)
        sparse_value = np.load('./data/traindata/sparse_value.npy', allow_pickle=True).astype('float32')
        sparse_index_row = sparse_index[:, 0]
        sparse_index_col = sparse_index[:, 1]
        sparse_matrix = sparse.coo_matrix((sparse_value, (sparse_index_row, sparse_index_col)),
                                          shape=(self.user_num, X.shape[0]))
        # 6527 * len(X)
        user_clip = sparse_matrix.todense()
        # 6527 * 4096
        user_history = np.matmul(user_clip, X)
        self.user_average_history = user_history
        self.history = X[6527:]
        for k, v in sparse_index:
            self.user_history[k].append(v-6527)
        return user_history

    def obtain_average_history(self):
        assert self.user_average_history is not None, "load data first"
        return self.user_average_history

    def distance_vector(self, user_id, clips, k=5):
        """

        Parameters
        ---------
        user_id: int, user id (from 0)
        clips: np.array (N, 4096)
        k: int,
        """
        result = np.zeros((clips.shape[0], k), dtype=np.float32)
        if user_id in self.user_history.keys():
            history_ind = self.user_history[user_id]
            # M*4096
            histotries = self.history[history_ind]
            # N*M
            dist = cdist(clips, histotries)
            dist = np.sort(dist, axis=1)[:, :k]
            result[:, :dist.shape[1]] = dist
        return result


def load_train_video_list():
    all_video_list = set(np.load('./data/traindata/videoset.npy', allow_pickle=True))
    test_video_list = set(np.load('./data/testdata/test_video.npy', allow_pickle=True))
    valid_video_list = set(np.load('./data/validation_videos.npy', allow_pickle=True))
    train_video_list = list(all_video_list.difference(test_video_list).difference(valid_video_list))
    return train_video_list


def load_validation_video_list():

    valid_video_list = np.load('./data/validation_videos.npy', allow_pickle=True).tolist()
    return valid_video_list


def load_test_video_list():
    test_video_list = np.load('./data/testdata/test_video.npy', allow_pickle=True).tolist()
    return test_video_list


def load_pos_neg_clips():
    video_clip_pos = np.load('./data/traindata/video_clip_pos.npy', allow_pickle=True).tolist()
    video_clip_neg = np.load('./data/traindata/video_clip_neg.npy', allow_pickle=True).tolist()
    return video_clip_pos, video_clip_neg


def load_user_video_clips():
    video_clip_user = np.load('./data/traindata/video_clip_user.npy', allow_pickle=True).tolist()
    return video_clip_user


def load_user_id():
    user_id = np.load('./data/traindata/user_id.npy', allow_pickle=True).tolist()
    return user_id


def load_test_data():
    test_data = np.load('./data/testdata/test_data.npy', allow_pickle=True).tolist()
    return test_data


def load_validation_data():
    validation_data = np.load('./data/validation_data.npy', allow_pickle=True).tolist()
    return validation_data


class Dataset:

    def __init__(self):
        self._data = []
        self.user_id = load_user_id()
        self._load_features()
        self._load_data()

    def _load_features(self):
        self.features = np.load('./data/traindata/all_features_2nor.npy', allow_pickle=True).tolist()

    def _load_data(self):
        cache_file = './data/train_data.cache'
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as f:
                self._data = pickle.load(f)
            print("load cache from:{}".format(cache_file))
            return

        video_list = load_train_video_list()
        video_pos_clips, video_neg_clips = load_pos_neg_clips()
        user_video_clips = load_user_video_clips()
        data = []
        for k in range(len(video_list)):
            video_id = video_list[k]
            all_clips = set()
            for vpc in video_pos_clips[video_id]:
                all_clips.add(vpc[0])
            for vnc in video_neg_clips[video_id]:
                all_clips.add(vnc[0])

            current_user_clips = user_video_clips[video_id]

            for u in current_user_clips.keys():
                user_id = self.user_id[u]
                sampled_clips = []
                current_user_clips_set = set([x[0] for x in current_user_clips[u]])
                current_user_neg_clips = list(all_clips.difference(current_user_clips_set))
                random.shuffle(current_user_neg_clips)
                current_user_clips_set = list(current_user_clips_set)
                min_len = min(len(current_user_neg_clips), len(current_user_clips_set))
                for idx in range(min_len):
                    posid = video_id + '+' + str(current_user_clips_set[idx])
                    negid = video_id + '+' + str(current_user_neg_clips[idx])
                    if negid in self.features.keys() and posid in self.features.keys():
                        sampled_clips.append((user_id, posid, negid))
                random.shuffle(sampled_clips)
                data += sampled_clips[:5]

        self._data = data
        with open(cache_file, 'wb') as f:
            pickle.dump(data, f)
            print("save to cache:{}".format(cache_file))

    def __len__(self):
        return len(self._data)

    def __getitem__(self, idx):
        user_id, pos_id, neg_id = self._data[idx]
        pos_feature = self.features[pos_id][0].astype('float32')
        neg_feature = self.features[neg_id][0].astype('float32')

        return user_id, pos_feature, neg_feature


# validation data loaders
class ValidationDataset:

    def __init__(self):
        self._data = []
        self.user_id = load_user_id()
        self._load_features()
        self._load_data()

    def _load_features(self):
        self.features = np.load('./data/traindata/all_features_2nor.npy', allow_pickle=True).tolist()

    def _load_data(self):
        cache_file = './data/validate_data.cache'
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as f:
                self._data = pickle.load(f)
            print("load cache from:{}".format(cache_file))
            return

        video_list = load_validation_video_list()
        video_pos_clips, video_neg_clips = load_pos_neg_clips()
        user_video_clips = load_user_video_clips()
        data = []

        for k in range(len(video_list)):
            video_id = video_list[k]
            all_clips = set()
            for vpc in video_pos_clips[video_id]:
                all_clips.add(vpc[0])
            for vnc in video_neg_clips[video_id]:
                all_clips.add(vnc[0])

            current_user_clips = user_video_clips[video_id]

            for u in current_user_clips.keys():
                user_id = self.user_id[u]
                sampled_clips = []
                current_user_clips_set = set([x[0] for x in current_user_clips[u]])
                current_user_neg_clips = list(all_clips.difference(current_user_clips_set))
                current_user_clips_set = list(current_user_clips_set)
                min_len = min(len(current_user_neg_clips), len(current_user_clips_set))
                for idx in range(min_len):
                    posid = video_id + '+' + str(current_user_clips_set[idx])
                    negid = video_id + '+' + str(current_user_neg_clips[idx])
                    if negid in self.features.keys() and posid in self.features.keys():
                        sampled_clips.append((user_id, posid, negid))
                random.shuffle(sampled_clips)
                data += sampled_clips[:5]

        self._data = data
        with open(cache_file, 'wb') as f:
            pickle.dump(data, f)
            print("save to cache:{}".format(cache_file))

    def __len__(self):
        return len(self._data)

    def __getitem__(self, idx):
        user_id, pos_id, neg_id = self._data[idx]
        pos_feature = self.features[pos_id][0].astype('float32')
        neg_feature = self.features[neg_id][0].astype('float32')

        return user_id, pos_feature, neg_feature


def collate(batch):
    user_ids = [x[0] for x in batch]
    pos = np.stack([x[1] for x in batch])
    neg = np.stack([x[2] for x in batch])

    return user_ids, pos, neg


if __name__ == '__main__':
    from IPython import embed

    dataset = Dataset()
    embed()
