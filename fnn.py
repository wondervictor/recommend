# FNN model
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.layers import Dense
import datasets


def fnn(segment, history):
    """
    Parameters
    ----------
    segment: (N, D)
    history: (N, D)

    Return
    ----------

    """
    x = tf.concat([segment, history], axis=1)
    with tf.variable_scope('fnn'):
        fc1 = Dense(512, activation=None,
                    kernel_initializer=tf.random_normal_initializer(stddev=1e-2),
                    name='fc1')
        out = fc1(x)
        out = tf.nn.relu(out)

        fc2 = Dense(64, activation=None,
                    kernel_initializer=tf.random_normal_initializer(stddev=1e-2),
                    name='fc2')
        out = fc2(out)
        out = tf.nn.relu(out)
        # out = tf.nn.dropout(out, rate=0.5)

        fc3 = Dense(1, activation=None,
                    kernel_initializer=tf.random_normal_initializer(stddev=1e-2),
                    name='score')
        score = fc3(out)
        # score = tf.nn.sigmoid(score)

    return score


def compute_loss(score_pos, score_neg):

    # margin = 0.
    # diff = score_pos - score_neg - margin
    # diff_sigmoid = tf.nn.sigmoid(diff)
    loss = tf.log(1+tf.exp(score_neg-score_pos))
    acc = tf.reduce_mean(tf.cast(score_pos > score_neg, dtype=tf.float32))
    loss = tf.reduce_mean(loss)
    tf.summary.scalar('train/accuracy', acc)
    return loss, acc


def infer_test(score_pos, score_neg):

    margin = 0.
    # diff = score_pos - score_neg - margin
    # diff_sigmoid = tf.nn.sigmoid(diff)
    loss = tf.log(1+tf.exp(score_neg-score_pos))
    acc = tf.reduce_mean(tf.cast(score_pos > score_neg, dtype=tf.float32))
    loss = tf.reduce_mean(loss)
    return loss, acc


def weight_decay_param_filter(name):
    if name.find('kernel') != -1 or name.find('weights') != -1:
        return True
    else:
        return False


def create_optimizer(loss, lr, global_step, weight_decay=1e-4):
    """
    Parameters
    ----------
    loss: op
    lr: op
    global_step: op
    weight_decay: float, default=0.001

    Return
    ----------

    """
    optimizer = tf.train.AdamOptimizer(lr)
    weight_decay_loss = weight_decay * tf.add_n(
        [tf.nn.l2_loss(tf.cast(v, tf.float32))
         for v in tf.trainable_variables()
         if weight_decay_param_filter(v.name)]
    )
    all_loss = loss + weight_decay_loss
    # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    # with tf.control_dependencies(update_ops):
    train_op = optimizer.minimize(all_loss, global_step)

    tf.summary.scalar('train/rank_loss', loss)
    tf.summary.scalar('train/learning_rate', lr)
    tf.summary.scalar('train/weight decay loss', weight_decay_loss)

    return all_loss, train_op


def get_learning_rate(epoch):
    base_lr = 1e-4
    # lr = 0.5**(epoch//10) * base_lr
    return base_lr


def train(num_epochs=16, batchsize=100):

    dataset = datasets.Dataset()
    num_samples = len(dataset)
    sample_indices = np.arange(0, num_samples)
    history_vector_helper = datasets.History()
    history_vector_helper.load_data()
    history_vectors = history_vector_helper.obtain_average_history()

    # validation set
    valida_dataset = datasets.ValidationDataset()
    num_valid_samples = len(valida_dataset)

    # placeholders
    pos_clip_input = tf.placeholder("float32", [None, 4096])
    neg_clip_input = tf.placeholder("float32", [None, 4096])
    history = tf.placeholder("float32", [None, 4096])
    learning_rate = tf.placeholder('float32', [], name='learning_rate')
    global_step = tf.Variable(0, name='global_step', trainable=False)

    with tf.variable_scope('fnn') as scope:
        score_pos = fnn(pos_clip_input, history)
        scope.reuse_variables()
        score_neg = fnn(neg_clip_input, history)

    rank_loss, acc = compute_loss(score_pos, score_neg)
    valid_loss, valid_acc = infer_test(score_pos, score_neg)
    # last_bias = tf.get_default_graph().get_tensor_by_name('fnn/fnn/score/bias:0')
    all_loss, train_op = create_optimizer(rank_loss, learning_rate, global_step)

    config = tf.ConfigProto()
    merged = tf.summary.merge_all()
    saver = tf.train.Saver(tf.global_variables())
    print(tf.trainable_variables())
    print("start training")
    print("num samples: ", len(dataset))
    with tf.Session() as sess:

        summary_writer = tf.summary.FileWriter('./log', sess.graph)
        sess.run(tf.global_variables_initializer())
        i = 0
        all_test_acc = []
        all_test_loss = []
        for epoch in range(num_epochs):
            lr = get_learning_rate(epoch)
            _losses = []

            np.random.shuffle(sample_indices)
            for idx in range(num_samples//batchsize-1):
                i += 1
                batch = [dataset[x] for x in sample_indices[idx*batchsize: (idx+1)*batchsize]]
                user_ids, pos_features, neg_features = datasets.collate(batch)
                history_features = np.take(history_vectors, user_ids, axis=0)
                _loss, _, rs = sess.run([rank_loss, train_op, merged],
                                        feed_dict={pos_clip_input: pos_features,
                                                   neg_clip_input: neg_features,
                                                   history: history_features,
                                                   learning_rate: lr})
                _losses.append(_loss)
                summary_writer.add_summary(rs, i)
            test_acc = []
            test_losses = []
            for idx in range(num_valid_samples // batchsize - 1):
                i += 1
                batch = [valida_dataset[x] for x in range(idx * batchsize, (idx + 1) * batchsize)]
                user_ids, pos_features, neg_features = datasets.collate(batch)
                history_features = np.take(history_vectors, user_ids, axis=0)
                _loss, _acc = sess.run([valid_loss, valid_acc],
                                       feed_dict={pos_clip_input: pos_features,
                                                 neg_clip_input: neg_features,
                                                 history: history_features})
                test_losses.append(_loss)
                test_acc.append(_acc)
            mean_test_loss = np.mean(test_losses)
            mean_test_acc = np.mean(test_acc)*100
            all_test_acc.append(mean_test_acc)
            all_test_loss.append(mean_test_loss)

            print("epoch: {}, training loss: {:.6f} validation loss: {:.6f} validation accuracy:{:.3f}%".format(
                epoch, np.mean(_losses), mean_test_loss, mean_test_acc))

            saver.save(sess, './save_model/fnn_model_epoch_{}'.format(epoch))

        plt.subplot(211)
        plt.title('Validation Loss')
        plt.plot(all_test_loss, '-o')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')

        plt.subplot(212)
        plt.title('Validation Accuracy')
        plt.plot(all_test_acc, '-x')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.savefig('result.pdf')
        plt.show()


if __name__ == '__main__':

    train(10, 100)



