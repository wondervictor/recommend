# -*- coding: utf-8 -*-


import os
import time
import numpy as np
import collections
import pdb
import tensorflow as tf
from tensorflow.python import debug as tf_debug
import sys
import codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())



t0 = time.time()
#all_features = np.load('../data/traindata/all_features_2.npy').tolist()   #read features:dimension=4096
all_features_nor = np.load('./data/traindata/all_features_2nor.npy', allow_pickle=True).tolist()
#for key in all_features.keys():
#    np_tmp = all_features[key][0]
#    np_out = (np_tmp-min(np_tmp))/(max(np_tmp)-min(np_tmp))
#    np_out = np.reshape(np.asarray(np_out), [-1, 4096])
#    all_features_nor[key] = np_out.astype('float16')
#np.save('../data/traindata/all_features_2nor.npy',all_features_nor)
all_video = np.load('./data/traindata/videoset.npy', allow_pickle=True).tolist()
video_clip_user = np.load('./data/traindata/video_clip_user.npy', allow_pickle=True).tolist()
video_clip_pos = np.load('./data/traindata/video_clip_pos.npy', allow_pickle=True).tolist()
video_clip_neg = np.load('./data/traindata/video_clip_neg.npy', allow_pickle=True).tolist()
#pdb.set_trace()

test_data = np.load('./data/testdata/test_data.npy', allow_pickle=True).tolist()   #read test_data:user_movie
test_video = np.load('./data/testdata/test_video.npy', allow_pickle=True).tolist()
test_features = np.load('./data/testdata/test_features/test_features_2.npy', allow_pickle=True).tolist()   #for accurate predict score
ground_truth = np.load('./data/testdata/ground_truth.npy', allow_pickle=True).tolist()   #for accurate map


train_video = list(set(all_video).difference(set(test_video)))
test_features_nor = np.load('./data/testdata/test_features_2nor.npy', allow_pickle=True).tolist()
#for key in test_features.keys():
#    np_tmp = test_features[key][0]
#    np_out = (np_tmp-min(np_tmp))/(max(np_tmp)-min(np_tmp))
#    np_out = np.reshape(np.asarray(np_out), [-1, 4096])
#    test_features_nor[key] = np_out.astype('float16')
#np.save('../data/testdata/test_features_2nor.npy', test_features_nor)
t1 = time.time()
print('*****数据读取完毕*****')
print('cost time:',round(t1-t0,4))#计算读取时间保留4位小数
    
##聚合
def get_traindata(all_features_512_i, video_clip_pos_i, video_clip_neg_i, videoid_i):
    list_pos = []
    list_neg = []
    for k in range(len(videoid_i)):
        video_tmp = videoid_i[k]                                        # 获取每一个video_id
        for i in range(len(video_clip_pos_i[video_tmp])):
            start_time = video_clip_pos_i[video_tmp][i][0]              # 获取video_clip_pos即正样本中的开始时间
            key = video_tmp +'+'+ str(start_time)                       # 令key = video_id+开始时间的形式
            if key not in all_features_512_i.keys():                    # 如果这个key不是视频特征文件的key值
                pass                                                    # 跳过
            else:                                                       # 否则
                feature = all_features_512_i[key]                       # 令feature为key所对应的value
                for j in range(len(video_clip_neg_i[video_tmp])):
                    start_time_1 = video_clip_neg_i[video_tmp][j][0]    # 获取video_clip_neg即负样本中的开始时间
                    key1 = video_tmp +'+'+ str(start_time_1)            # 令key1为同上形式
                    if key1 not in all_features_512_i.keys():
                        pass
                    else:
                        feature1 = all_features_512_i[key1]
                        list_pos.append(feature)                        # 将feature放入列表pos中
                        list_neg.append(feature1)                       # 将feature1放入列表neg中
#        for i in range(len(video_clip_neg_i[video_tmp])):
#            start_time = video_clip_neg_i[video_tmp][i][0]
#            key = videoid_i + str(start_time)
#            feature = all_features_512_i[key]
#            list_neg.append(feature)
    return np.reshape(np.asarray(list_pos),[-1,4096]),np.reshape(np.asarray(list_neg),[-1,4096])        # reshape上述两个列表，为[有多少行是多少， 列4096]

# os.environ["CUDA_VISIBLE_DEVICES"] = "0" # 选gpu设备0
epochs = 150 # 训练150次
learning_rate = 0.001 # 学习率0.001，保持较好的loss损失函数
batch_size = 100 # 一次训练100个数据
w1 =  tf.Variable(tf.random_normal([4096, 512], stddev=0.01, dtype='float16'), name='w1')               #定义变量w1服从正态分布，shape=[]，标准差0.01，类型float6,hidden layer,w为权重，无b, fnn为wTx + b
w2 =  tf.Variable(tf.random_normal([512, 128], stddev=0.01, dtype='float16'), name='w2')                #同上
w3 =  tf.Variable(tf.random_normal([128, 1], stddev=0.01, dtype='float16'), name='w3')                  #同上
pos_clip_input = tf.placeholder("float16", [None, 4096])                                                #placeholder，hold这个place
neg_clip_input = tf.placeholder("float16", [None, 4096])

def compute_loss(w1_i, w2_i, w3_i, pos_clip_input_i, neg_clip_input_i):
    h1_pos = tf.nn.relu(tf.matmul(pos_clip_input_i, w1_i))  #1x4096*4096x512 = 1x512                    #激活hidden layer
    h2_pos = tf.nn.relu(tf.matmul(h1_pos, w2_i))  #1X128
    score_pos = tf.matmul(h2_pos, w3_i)  #1X1
    h1_neg = tf.nn.relu(tf.matmul(neg_clip_input_i, w1_i))
    h2_neg = tf.nn.relu(tf.matmul(h1_neg, w2_i))
    score_neg = tf.matmul(h2_neg, w3_i)
    out_auc = tf.reduce_mean(tf.to_float(score_pos-score_neg>0))
    #计算score_pos-score_neg>0均值,对应论文实施方案中的目标是sc+ - sc->0，>0时正确
#    a = -tf.reduce_mean(tf.log(tf.clip_by_value(tf.nn.sigmoid(score_pos-score_neg),1e-10,1.0)))
    out_pairloss = -tf.reduce_sum(tf.log(tf.clip_by_value(tf.nn.sigmoid(score_pos-score_neg),1e-7,1.0)))
    #bpr计算方法，交叉熵，衡量两个概率分布的差异性，获得损失函数，使用sigmoid函数在梯度下降时能避免均方误差损失函数学习速率降低的问题；y=1/(1 + exp (score_pos-score_neg)),当y小于1e-7时，输出1e-7；当y大于1e-7小于1.0时，输出原值；当y大于1.0时，输出1.0；z=loge(1 + y)并求和，sigmoid用作输出压缩至0~1之间作为预测
    regulation = 0.8*tf.reduce_sum(tf.square(w1_i))+0.25*tf.reduce_sum(tf.square(w2_i))+0.25*tf.reduce_sum(tf.square(w3_i))     #正则化，防止模型过拟合
#    out_pairloss = -tf.reduce_sum(tf.log(max(tf.nn.sigmoid(score_pos-score_neg),1e-8)))
    return out_auc,out_pairloss,regulation
auc,loss1,loss2 = compute_loss(w1, w2, w3, pos_clip_input, neg_clip_input)								#损失函数
opt = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss1)                                    #损函及优化器选择
#opt = tf.train.AdamOptimizer(learning_rate).minimize(loss)


# start tensorflow session
init = tf.global_variables_initializer()    #初始化
config = tf.ConfigProto()                   #参数设置
config.gpu_options.allow_growth = True      #分配器将不会指定所有的GPU内存
sess = tf.Session(config=config)
sess.run(init)  
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)

def train_batch(train_video_i, batch_size, start_index_i):
    end_index = min(start_index_i+batch_size, len(train_video_i))       #啥时候结束
    out_list = []
    for i in range(start_index_i, end_index):
        out_list.append(train_video_i[i])                               #训练的视频id放列表
    return out_list, end_index

for epoch in range(epochs):
    
    file = open('./result/fnn_loss.txt','w')
    ### train ###
    auc_sum = 0
    loss_sum = 0
    epoch_sum = 0
    print('*****epoch*****:',epoch)
    w11,w22,w33 = sess.run([w1,w2,w3])                    #初始化w1w2w3
    for k in range(int(len(train_video)/batch_size)+1):
        start_index = k*batch_size
        end_index = min((k+1)*batch_size, len(train_video))
        vid_list = train_video[start_index:end_index]
        train_data_pos, train_data_neg = \
        get_traindata(all_features_nor, video_clip_pos, video_clip_neg, vid_list)
        _auc,_loss1,_loss2,_ = sess.run([auc,loss1,loss2,opt], \
                              feed_dict={pos_clip_input:train_data_pos, neg_clip_input:train_data_neg})
#        pdb.set_trace()
#        w1_,w2_,w3_ = sess.run([w1,w2,w3])
        auc_sum += _auc * len(train_data_pos)
        loss_sum += _loss1
        epoch_sum += len(train_data_pos)
    train_auc = auc_sum/epoch_sum
    train_loss = loss_sum/epoch_sum
    print('train_auc:',round(train_auc,4),'train_loss',round(train_loss,4))  
    file.write('epoch:'+str(epoch)+' '+'train_auc:'+ \
               str(round(train_auc,4))+' '+'train_loss'+str(round(train_loss,4))+'\n')
    ### test ###
    test_data_pos, test_data_neg = \
    get_traindata(all_features_nor, video_clip_pos, video_clip_neg, test_video)
    _auc,_loss1,_loss2 = sess.run([auc,loss1,loss2], \
                                  feed_dict={pos_clip_input:train_data_pos, neg_clip_input:train_data_neg})
    test_auc = _auc
    test_loss = _loss1/len(test_data_pos)
    print('test_auc:',round(test_auc,4),'test_loss',round(test_loss,4))
    file.write('epoch:'+str(epoch)+' '+'test_auc:'+ \
               str(round(test_auc,4))+' '+'test_loss'+str(round(test_loss,4))+'\n')

file.close()
saver = tf.train.Saver()
save_path = saver.save(sess, "./save_model/fnn/fnn_premodel.ckpt")
