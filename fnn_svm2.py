# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 15:40:59 2019

@author: Administrator
"""

import os
import time
import numpy as np
import collections
import pdb
import math
import evaluate
import tensorflow as tf
from tensorflow.python import debug as tf_debug

pretrained_flag = 0
device_id = 0
epochs = 50
batch_size = 100
learning_rate = 0
lamda = 0.1
num_user = 6527
num_video = 5137
num_clip = 57299

pre_model = './save_model/100epoch/epoch_7_nscg_0.1781_map_0.217.ckpt'

if pretrained_flag == 0:
    file = open('./result/loss1.txt', 'w')
    file.write('********************' + '\n')
    file.write('\n')
    file.write('learning_rate=' + str(learning_rate) + ';' + 'lamda=' + str(lamda) + ';' + '\n')

t0 = time.time()

## data for train ##
# all_features = np.load('../data/traindata/all_features_2.npy').tolist()   #read features:dimension=4096
"""
all_features_nor = np.load('./data/traindata/all_features_2nor.npy', allow_pickle=True).tolist()
all_video = np.load('./data/traindata/videoset.npy', allow_pickle=True).tolist()
video_clip_user = np.load('./data/traindata/video_clip_user.npy', allow_pickle=True).tolist()
video_clip_pos = np.load('./data/traindata/video_clip_pos.npy', allow_pickle=True).tolist()
video_clip_neg = np.load('./data/traindata/video_clip_neg.npy', allow_pickle=True).tolist()
video_infornation = np.load('./data/testdata/video_information.npy', allow_pickle=True).tolist()
ui_highlight_exist = np.load('./data/traindata/ui_highlight_exist.npy', allow_pickle=True).tolist()
ui_hlight_concat = np.load('./data/traindata/video_user_allhlight_concat.npy', allow_pickle=True).tolist()

## data for test ##
test_data = np.load('./data/testdata/test_data.npy', allow_pickle=True).tolist()  # read test_data:user_movie
test_video = np.load('./data/testdata/test_video.npy', allow_pickle=True).tolist()
ground_truth = np.load('./data/testdata/ground_truth.npy', allow_pickle=True).tolist()  # for accurate map
test_features_nor = np.load('./data/testdata/test_features_2nor.npy', allow_pickle=True).tolist()
train_video = list(set(all_video).difference(set(test_video)))

user_id = np.load('./data/traindata/user_id.npy', allow_pickle=True).tolist()
video_id = np.load('./data/traindata/video_id.npy', allow_pickle=True).tolist()
clip_id = np.load('./data/traindata/clip_id.npy', allow_pickle=True).tolist()
X = np.load('./data/traindata/X_matrix.npy', allow_pickle=True).tolist()
sparse_index = np.load('./data/traindata/sparse_index.npy', allow_pickle=True).tolist()
sparse_value = np.load('./data/traindata/sparse_value.npy', allow_pickle=True).tolist()
len_graph = len(X)
"""

t1 = time.time()

print('reading dataset cost time:', round(t1 - t0, 4))

def get_traindata():
    pass

def get_traindata1():
    video_test_user = {}
    for key in test_data.keys():
        video = test_data[key]
        video_test_user[video] = key  ##video_test_user为video:user
    user_list = []
    clip_pos_list = []
    clip_neg_list = []
    for i in range(len(test_video)):
        video = test_video[i]  ##获取test_video中的video
        allclip = set()
        for i in range(len(video_clip_pos[video])):
            allclip.add(video_clip_pos[video][i][0])
        for i in range(len(video_clip_neg[video])):
            allclip.add(video_clip_neg[video][i][0])
        for u in video_clip_user[video].keys():
            if u != video_test_user[video]:  ##user_video not in testset
                uid = user_id[u]
                clip_u = set()
                for j in range(len(video_clip_user[video][u])):
                    clip_u.add(video_clip_user[video][u][j][0])  ##pos clip for user:u
                for p in clip_u:
                    posid = video + '+' + str(p)
                    for n in allclip:
                        negid = video + '+' + str(n)
                        if (n not in clip_u) and (negid in all_features_nor.keys()) and (
                                posid in all_features_nor.keys()):
                            user_list.append([uid])
                            clip_pos_list.append(all_features_nor[posid])
                            clip_neg_list.append(all_features_nor[negid])
    return np.reshape(np.asarray(user_list), [-1, 1]), np.reshape(np.asarray(clip_pos_list).astype(np.float32),
                                                                  [-1, 4096]), np.reshape(
        np.asarray(clip_neg_list).astype(np.float32), [-1, 4096])


# function for test_auc、hr、ndcg
def get_testdata():
    pass


# test_user_pn_clips = get_testdata()
# pdb.set_trace()

# some funtion for test_map
def get_gt_score_personized():
    '''
     This function shows how a method can be evaluated personized
    '''
    gt_score = {}  # ground_truth matrix
    for key in ground_truth.keys():
        #        user = key
        video = test_data[key]
        fps = video_infornation[video]['fps']
        frames = video_infornation[video]['frames']
        gt_score_tmp = np.zeros(frames)  # chushihua:0
        for i in range(len(ground_truth[key])):
            start_frame = int(ground_truth[key][i][0] * fps)
            end_frame = min(int(ground_truth[key][i][1] * fps), frames)
            gt_score_tmp[start_frame:end_frame] = 1
        gt_score[key] = gt_score_tmp
    return gt_score


def get_testclip():  # get {video:[clip1,clip2,...,clipk]}
    video_key = {}
    for i in range(len(test_video)):
        list_tmp = []
        video = test_video[i]
        duration = video_infornation[video]['duration']
        max_k = int(duration / 5)
        if duration % 5 == 0:
            max_k = max_k - 1
        for k in range(max_k + 1):
            list_tmp.append(video + '+' + str(5 * k))
        video_key[video] = list_tmp
    return video_key


def test_personized():
    test_clip = get_testclip()
    gt_score = get_gt_score_personized()  # personized
    all_ap = np.zeros(len(gt_score))
    all_msd = np.zeros(len(gt_score))
    k = 0
    #    pdb.set_trace()
    for key in gt_score.keys():
        user_list = []
        clip_features = []
        u_tmp = user_id[key]
        video = test_data[key]
        clip_keys = test_clip[video]
        frames = video_infornation[video]['frames']
        fps = video_infornation[video]['fps']
        score_tmp = np.zeros(frames)
        for i in range(len(clip_keys)):
            if clip_keys[i] not in test_features_nor.keys():
                clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                user_list.append([u_tmp])
            else:
                clip_features.append(test_features_nor[clip_keys[i]])
                user_list.append([u_tmp])
        user_list = np.reshape(np.asarray(user_list), [-1, 1])
        clip_features = np.reshape(np.asarray(clip_features), [-1, 4096]).astype(np.float32)
        _test_score = np.reshape(np.asarray(sess.run(final_prediction, \
                                                     feed_dict={test_user_input: user_list,
                                                                test_clip_input: clip_features})), [-1, 1])
        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2
                #        test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        test_score_tmp = _test_score
        for i in range(len(clip_keys)):
            start_frame = min(int(i * 5 * fps), frames)
            end_frame = min(int((i + 1) * 5 * fps), frames)
            score_tmp[start_frame:end_frame] = test_score_tmp[i][0]
        score_fnn = score_tmp
        score_gt = gt_score[key]
        all_ap[k] = evaluate.get_ap(score_gt, score_fnn)
        all_msd[k] = evaluate.meaningful_summary_duration([score_gt], score_fnn)
        k += 1
    print('AP=%.2f%%; MSD=%.2f%%' % (100 * np.mean(all_ap), 100 * np.mean(all_msd)))
    return round(100 * np.mean(all_ap), 2), round(100 * np.mean(all_msd), 2)


def generate_hr_ndcg():
    test_clip = get_testclip()
    gt_score = get_gt_score_personized()  # key is user
    user_list = []
    pos_prediction = {}
    neg_prediction = {}

    for key in gt_score.keys():
        pos_user_list = []
        neg_user_list = []
        tmp_u = user_id[key]
        tmp_pos_prediction = []
        tmp_neg_prediction = []
        video = test_data[key]  # user->
        all_clip_keys = test_clip[video]  # video->all clips(video+time)
        pos_clip_id = video_clip_user[video][key]
        pos_clip_keys = []
        for i in range(len(pos_clip_id)):
            #            pdb.set_trace()
            pos_clip_keys.append(video + '+' + str(pos_clip_id[i][0]))  # add posclip
            if video + '+' + str(pos_clip_id[i][0]) in all_clip_keys:
                all_clip_keys.remove(video + '+' + str(pos_clip_id[i][0]))  # remove posclip
        #### pos prediction ####
        pos_clip_features = []
        for i in range(len(pos_clip_keys)):
            if pos_clip_keys[i] not in test_features_nor.keys():
                pos_clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                pos_user_list.append([tmp_u])
            else:
                pos_clip_features.append(test_features_nor[pos_clip_keys[i]])
                pos_user_list.append([tmp_u])
        pos_clip_features = np.reshape(np.asarray(pos_clip_features), [-1, 4096]).astype(np.float32)
        #        pdb.set_trace()
        _test_score = np.reshape(np.asarray(sess.run(final_prediction, \
                                                     feed_dict={test_user_input: np.reshape(np.asarray(pos_user_list),
                                                                                            [-1, 1]), \
                                                                test_clip_input: pos_clip_features})), [-1, 1])
        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2
                #        if len(_test_score) > 0:
        #            test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        for i in range(len(_test_score)):
            tmp_pos_prediction.append(_test_score[i][0])
        #### neg prediction ####
        neg_clip_keys = all_clip_keys
        neg_clip_features = []
        for i in range(len(neg_clip_keys)):
            if neg_clip_keys[i] not in test_features_nor.keys():
                neg_clip_features.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
                neg_user_list.append([tmp_u])
            else:
                neg_clip_features.append(test_features_nor[neg_clip_keys[i]])
                neg_user_list.append([tmp_u])
        neg_clip_features = np.reshape(np.asarray(neg_clip_features), [-1, 4096]).astype(np.float32)
        _test_score = np.reshape(np.asarray(sess.run(final_prediction, \
                                                     feed_dict={test_user_input: np.reshape(np.asarray(neg_user_list),
                                                                                            [-1, 1]), \
                                                                test_clip_input: neg_clip_features})), [-1, 1])
        for i in range(len(_test_score)):
            if _test_score[i][0] == 0:
                if i == 0:
                    _test_score[i][0] = _test_score[i + 1][0]
                elif i == len(_test_score) - 1:
                    _test_score[i][0] = _test_score[i - 1][0]
                else:
                    _test_score[i][0] = (_test_score[i - 1][0] + _test_score[i + 1][0]) / 2

                #        if len(_test_score) > 0:
        #            test_score_tmp = (_test_score-min(_test_score))/(max(_test_score)-min(_test_score))
        for i in range(len(_test_score)):
            tmp_neg_prediction.append(_test_score[i][0])
        #### add to dict ####
        user_list.append(key)
        pos_prediction[key] = tmp_pos_prediction
        neg_prediction[key] = tmp_neg_prediction
    return user_list, pos_prediction, neg_prediction


def get_idcg(length):
    idcg = 0.0
    for i in range(length):
        idcg = idcg + math.log(2) / math.log(i + 2)
    return idcg


def compute_hr_ndcg(user_list, pos_prediction, neg_prediction, topk):
    user_map_list = []
    user_nmsd_list = []
    user_hr_list = []
    user_ndcg_list = []
    user_recall_list = []
    for i in range(len(user_list)):
        tmp_user = user_list[i]
        pos_length = len(pos_prediction[tmp_user])
        target_length = min(topk, pos_length)

        pos_value = pos_prediction[tmp_user]
        neg_value = neg_prediction[tmp_user]
        pos_value.extend(neg_value)
        # map and nmsd
        score_predict = np.asarray(pos_value)
        score_predict = (score_predict - min(score_predict)) / (max(score_predict) - min(score_predict))
        score_gt = np.zeros(len(score_predict))
        score_gt[:pos_length] = 1
        user_map_list.append(evaluate.get_ap(score_gt, score_predict))
        user_nmsd_list.append(evaluate.meaningful_summary_duration([score_gt], score_predict))

        tmp_prediction = np.asarray(pos_value)
        sort_index = np.argsort(tmp_prediction)[::-1]  # sort index
        hit_value = 0
        dcg_value = 0
        for idx in range(min(topk, len(sort_index))):
            ranking = sort_index[idx]
            if ranking < pos_length:
                hit_value += 1
                dcg_value += math.log(2) / math.log(idx + 2)
        tmp_hr = round(hit_value / target_length, 4)
        idcg = get_idcg(target_length)
        tmp_dcg = round(dcg_value / idcg, 4)
        tmp_recall = hit_value / pos_length
        user_hr_list.append(tmp_hr)
        user_ndcg_list.append(tmp_dcg)
        user_recall_list.append(tmp_recall)

    mean_map = sum(user_map_list) / len(user_list)
    mean_nmsd = sum(user_nmsd_list) / len(user_list)
    mean_hr = sum(user_hr_list) / len(user_list)
    mean_ndcg = sum(user_ndcg_list) / len(user_list)
    mean_recall = sum(user_recall_list) / len(user_list)
    return round(mean_map, 4), round(mean_nmsd, 4), round(mean_hr, 4), round(mean_ndcg, 4), round(mean_recall, 4)


"""
#################################################################
os.environ["CUDA_VISIBLE_DEVICES"] = str(device_id)
w1 = tf.Variable(tf.random_normal([4096, 512], stddev=0.01, dtype='float32'), name='w1')  ### three hidden layer ###
w2 = tf.Variable(tf.random_normal([512, 128], stddev=0.01, dtype='float32'), name='w2')
w3 = tf.Variable(tf.random_normal([128, 1], stddev=0.01, dtype='float32'), name='w3')
ws = tf.Variable(tf.random_normal([3, 1], stddev=0.01, dtype='float32'), name='ws')
b = tf.Variable(tf.random_normal([1, 1], stddev=0.01, dtype='float32'), name='bias')

### history record for fnn
sparse_A = tf.SparseTensor(indices=sparse_index, values=sparse_value,
                           dense_shape=np.asarray([len_graph, len_graph]).astype(np.int64))
X = np.reshape(np.asarray(X), [-1, 4096]).astype(np.float32)  # [len_graph,4096]
Y_fnn = tf.sparse_tensor_dense_matmul(sparse_A, X)  # [len_graph,4096]

### train part ###
user_input = tf.placeholder("int32", [None, 1])  # user
pos_clip_input = tf.placeholder("float32", [None, 4096])  # train pos
neg_clip_input = tf.placeholder("float32", [None, 4096])  # train neg

# fnn part #
user_history = tf.gather_nd(Y_fnn, user_input)
pos_fusion = tf.concat([pos_clip_input, user_history], axis=0)
neg_fusion = tf.concat([neg_clip_input, user_history], axis=0)
h1_pos = tf.nn.relu(tf.matmul(pos_fusion, w1))  # 1x512
h2_pos = tf.nn.relu(tf.matmul(h1_pos, w2))  # 1X128
score_pos = tf.matmul(h2_pos, w3)  # 1X1
h1_neg = tf.nn.relu(tf.matmul(neg_fusion, w1))
h2_neg = tf.nn.relu(tf.matmul(h1_neg, w2))
score_neg = tf.matmul(h2_neg, w3)

# svm part #
user_history_factor = np.array([])
pos_cosin = np.array([])
neg_cosin = np.array([])
history_clip = np.array([])
user_svm_factor = user_input
for id in sparse_index:
    if id[0] == user_svm_factor:
        clip_id = id[1]
        history_clip = np.append(history_clip, clip_id).astype(np.int32)
for id in history_clip:
    history_fea = X[id]
    user_history_factor = np.append(user_history_factor, history_fea).astype(np.float32)
for i in range(4):
    if len(user_history_factor) < 3:
        user_history_factor.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
for i in range(len(user_history_factor)):
    pos_cosin_dist1 = 1 - np.dot(user_history_factor[i], pos_clip_input) / (np.linalg.norm(user_history_factor[i]) * np.linalg.norm(pos_clip_input))
    neg_cosin_dist1 = 1 - np.dot(user_history_factor[i], neg_clip_input) / (np.linalg.norm(user_history_factor[i]) * np.linalg.norm(neg_clip_input))
    pos_cosin = np.append(pos_cosin, pos_cosin_dist1).astype(np.float32)
    neg_cosin = np.append(pos_cosin, neg_cosin_dist1).astype(np.float32)
    pos_cosin.sort()
    neg_cosin.sort()
    pos_cosin = np.reshape(np.asarray(pos_cosin), [1, -1])
    neg_cosin = np.reshape(np.asarray(neg_cosin), [1, -1])
    pos_factor = pos_cosin[-3:]
    neg_factor = neg_cosin[-3:]
Rai = tf.add(tf.matmul(pos_factor, ws), b)
Raj = tf.add(tf.matmul(neg_factor, ws), b)

# fusion layer #
final_pos = Rai + score_pos
final_neg = Raj + score_neg
auc = tf.reduce_mean(tf.to_float(final_pos - final_neg > 0))
bprloss = -tf.reduce_sum(tf.log(tf.clip_by_value(tf.nn.sigmoid(final_pos - final_neg), 1e-7, 1.0)))
regulation = lamda * (tf.reduce_sum(
    tf.square(ws) + 0.8 * tf.reduce_sum(tf.square(w1)) + 0.25 * tf.reduce_sum(tf.square(w2)) + 0.25 * tf.reduce_sum(
        tf.square(w3))))
loss = bprloss + regulation
opt = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
"""
# test part #
test_user_input = tf.placeholder("int32", [None, 1])  # test user
test_clip_input = tf.placeholder("float32", [None, 4096])  # test clip

user_factor = np.array([])
user_cosin = np.array([])
user_clip = np.array([])
for id in sparse_index:
    if id[0] == test_user_input:
        clip_id = id[1]
        user_clip = np.append(user_clip, clip_id).astype(np.int32)
for id in user_clip:
    user_fea = X[id]
    user_factor = np.append(user_factor, user_fea).astype(np.float32)
for i in range(3):
    if len(user_factor) < 3:
        user_factor.append(np.reshape(np.zeros(4096, dtype='float16'), [1, 4096]))
for i in range(len(user_factor)):
    user_cosin_dist1 = 1 - np.dot(user_factor[i], test_clip_input) / (np.linalg.norm(user_factor[i]) * np.linalg.norm(test_clip_input))
    user_cosin = np.append(user_cosin, user_cosin_dist1).astype(np.float32)
    user_cosin.sort()
    user_cosin = np.reshape(np.asarray(pos_cosin), [1, -1])
    user_factor = user_cosin[-3:]

prediction_svm = tf.add(tf.matmul(user_factor, ws), b)

clip_fusion = test_clip_input + tf.gather_nd(Y_fnn, test_user_input)
h1 = tf.nn.relu(tf.matmul(clip_fusion, w1))  # 1x512
h2 = tf.nn.relu(tf.matmul(h1, w2))  # 1X128
score_fnn = tf.matmul(h2, w3)  # 1X1
final_prediction = prediction_svm + score_fnn

# start tensorflow session
init = tf.global_variables_initializer()
saver = tf.train.Saver(tf.global_variables())
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
sess.run(init)
if pretrained_flag == 1:
    saver.restore(sess, pre_model)
# sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#################################################################


print('*****start train*****')
auc_list = []
map_list = []
nmsd_list = []
max_auc = 0
max_map = 0
min_nmsd = 100
max_hr = 0
max_ndcg = 0
max_recall = 0
for epoch in range(epochs):

    '''
    compute train loss and optimization
    '''
    # train process #
    t11 = time.time()
    auc_sum = 0
    loss1_sum = 0
    loss2_sum = 0
    epoch_sum = 0
    print('*****epoch*****:', epoch)
    for k in range(int(len(train_video) / batch_size) + 1):
        start_index = k * batch_size
        end_index = min((k + 1) * batch_size, len(train_video))
        vid_list = train_video[start_index:end_index]
        user_list, posclip_list, negclip_list = get_traindata(vid_list)
        _auc, _loss1, _loss2, _ = sess.run([auc, bprloss, regulation, opt], \
                                           feed_dict={user_input: user_list, pos_clip_input: posclip_list,
                                                      neg_clip_input: negclip_list})
        auc_sum += _auc * len(user_list)
        loss1_sum += _loss1
        loss2_sum += _loss2
        epoch_sum += len(user_list)

    user_list, posclip_list, negclip_list = get_traindata1()
    for k in range(int(len(user_list) / batch_size) + 1):
        start_index = k * batch_size
        end_index = min((k + 1) * batch_size, len(user_list))
        user_list_batch = user_list[start_index:end_index]
        pos_list_batch = posclip_list[start_index:end_index]
        neg_list_batch = negclip_list[start_index:end_index]
        _auc, _loss1, _loss2, _ = sess.run([auc, bprloss, regulation, opt], \
                                           feed_dict={user_input: user_list_batch, pos_clip_input: pos_list_batch,
                                                      neg_clip_input: neg_list_batch})
        auc_sum += _auc * len(user_list_batch)
        loss1_sum += _loss1
        loss2_sum += _loss2
        epoch_sum += len(user_list_batch)

    train_auc = auc_sum / epoch_sum
    train_loss1 = loss1_sum / epoch_sum
    train_loss2 = loss2_sum / epoch_sum
    print('train_auc:', round(train_auc, 4), 'train_loss1', round(train_loss1, 4), 'train_loss2', round(train_loss2, 4))

    ### test auc ###
    test_user_pn_clips = get_testdata()
    user_test = []
    clip_pos_test = []
    clip_neg_test = []
    for key in test_user_pn_clips.keys():
        uid = user_id[key]
        for i in range(len(test_user_pn_clips[key][0])):
            for j in range(len(test_user_pn_clips[key][1])):
                user_test.append([uid])
                clip_pos_test.append(test_features_nor[test_user_pn_clips[key][0][i]])
                clip_neg_test.append(test_features_nor[test_user_pn_clips[key][1][j]])
    user_test = np.reshape(np.asarray(user_test), [-1, 1])
    clip_pos_test = np.reshape(np.asarray(clip_pos_test), [-1, 4096]).astype(np.float32)
    clip_neg_test = np.reshape(np.asarray(clip_neg_test), [-1, 4096]).astype(np.float32)
    _testauc, _testloss1, _testloss2 = sess.run([auc, bprloss, regulation], \
                                                feed_dict={user_input: user_test, pos_clip_input: clip_pos_test,
                                                           neg_clip_input: clip_neg_test})
    _testauc = round(_testauc, 4)
    print('test_auc:', _testauc, 'test_loss1', round(_testloss1 / len(user_test), 4))
    max_auc = max(_testauc, max_auc)

    tt2 = time.time()
    for topk in range(1, 6):
        hit_user_list, hit_pos_prediction, hit_neg_prediction = generate_hr_ndcg()
        my_map, my_nmsd, _hr, _ndcg, _recall = compute_hr_ndcg(hit_user_list, hit_pos_prediction, hit_neg_prediction,
                                                               topk)
        print('test_map:', my_map, 'nmsd:', my_nmsd, 'hr:', _hr, 'ndcg:', _ndcg, 'recall:', _recall)

    ### test map ###
    _map, _nmsd = test_personized()
    print('_map:', _map)
    print('_nmsd:', _nmsd)
    max_map = max(max_map, my_map)
    min_nmsd = min(min_nmsd, my_nmsd)
    max_hr = max(max_hr, _hr)
    max_ndcg = max(max_ndcg, _ndcg)
    max_recall = max(max_recall, _recall)

    ### write ###
    if pretrained_flag == 0:
        file.write('epoch:' + str(epoch) + ' train loss:' + str(round(train_loss1, 4)) + '+' + str(
            round(train_loss2, 4)) + ' train auc:' + str(round(train_auc, 4)) + '\n')
        file.write(
            'test loss:' + str(round(_testloss1 / len(user_test), 4)) + ' test auc:' + str(round(_testauc, 4)) + '\n')
        file.write(' map:' + str(my_map) + ' nmsd:' + str(my_nmsd) + 'hr:' + str(_hr) + ' ndcg:' + str(
            _ndcg) + ' recall:' + str(_recall) + '\n')
        file.write('\n')
        #    if epoch > 3 and _ndcg==max_ndcg:
        saver.save(sess, './save_model/50epoch+linea/' + 'epoch' + '_' + str(epoch) + '_' + 'nscg' + '_' + str(
            _ndcg) + '_' + 'map' + '_' + str(my_map) + '.ckpt')

if pretrained_flag == 0:
    file.write('*********over*********' + '\n')
    file.write('max auc:' + str(max_auc) + ';' + ' max map:' + str(max_map) + ' min nmsd:' + str(min_nmsd) + '\n')
    file.write('max hr:' + str())

    # print('visual_dim:', visual_dim)
    print('max auc:', max_auc)
    print('max map:', max_map, '%')
    print('min nmsd:', min_nmsd, '%')
