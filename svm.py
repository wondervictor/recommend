# SVM model
import numpy as np
from sklearn import svm
import datasets
from sklearn.externals import joblib


class RankSVM:

    def __init__(self, max_iter=1000, verbose=False):
        self.classifier = svm.LinearSVC(
            max_iter=max_iter,
            verbose=verbose)
        self.trained = False
        self._coef = None
        self._bias = None

    def fit(self, X_pos, X_neg):
        """
        X_pos: np.array, (num_samples, features)
        X_neg: np.array, (num_samples, features)
        """
        # (class, features)
        num_samples = X_pos.shape[0]
        Y = np.random.randint(0, 2, (num_samples, 1))

        X = (X_pos - X_neg)*Y + (X_neg - X_pos)*(1-Y)
        self.classifier.fit(X, Y)
        self._coef = self.classifier.coef_
        self._bias = self.classifier.intercept_
        self.trained = True

    def save_model(self, path):
        joblib.dump(self.classifier, path)

    def load_model(self, path):
        self.classifier = joblib.load(path)
        self.trained = True
        self._coef = self.classifier.coef_
        self._bias = self.classifier.intercept_

    def predict(self, X):
        """
        Parameters
        ----------
        X: array, (num_samples, features)

        Returns
        ----------
        scores: array, (num_samples, 1)
        """
        assert self.trained, "Train Rank SVM first"
        scores = np.dot(X, self._coef.T) + self._bias
        return scores


def train():

    dataset = datasets.Dataset()
    num_samples = len(dataset)
    sample_indices = np.arange(0, num_samples)[:1000]
    history_vector_helper = datasets.History()
    history_vector_helper.load_data()

    samples = [dataset[i] for i in sample_indices]
    user_id, pos_features, neg_features = datasets.collate(samples)
    pos_features = [history_vector_helper.distance_vector(uid, pos[np.newaxis])
                    for uid, pos in zip(user_id, pos_features)]
    neg_features = [history_vector_helper.distance_vector(uid, neg[np.newaxis])
                    for uid, neg in zip(user_id, neg_features)]
    pos_features = np.concatenate(pos_features, axis=0)
    neg_features = np.concatenate(neg_features, axis=0)

    svm_model = RankSVM(verbose=True)
    svm_model.fit(pos_features, neg_features)
    svm_model.save_model('save_model/svm.pkl')

    # test
    valid_dataset = datasets.ValidationDataset()
    num_valid_samples = len(valid_dataset)
    sample_indices = np.arange(0, num_valid_samples)[:1000]
    valid_samples = [valid_dataset[i] for i in sample_indices]

    user_id, pos_features, neg_features = datasets.collate(valid_samples)
    pos_features = [history_vector_helper.distance_vector(uid, pos[np.newaxis])
                    for uid, pos in zip(user_id, pos_features)]
    neg_features = [history_vector_helper.distance_vector(uid, neg[np.newaxis])
                    for uid, neg in zip(user_id, neg_features)]
    pos_features = np.concatenate(pos_features, axis=0)
    neg_features = np.concatenate(neg_features, axis=0)
    pos_score = svm_model.predict(pos_features)
    neg_score = svm_model.predict(neg_features)
    acc = (pos_score > neg_score).mean()
    print("SVM valdiation accuracy:{:.4f}%".format(acc*100))


if __name__ == '__main__':

    train()